#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='\w $ '
alias aur='pacman -Qqem'
alias dpi-='xrandr --dpi 96'
alias dpi+='xrandr --dpi 120'
alias dpi='xdpyinfo | grep -B2 resolution'
alias ls='exa --group-directories-first'
alias λσ='exa --group-directories-first'
alias config='/usr/bin/git --git-dir=/home/christos/.dotfiles/ --work-tree=/home/christos'
alias info='pacman -Qi'
alias rr='curl -s -L http://bit.ly/10hA8iC | bash'
alias listorph="pacman -Qdttq"
alias yay='paru'
alias παρθ='paru'
alias reflect='sudo reflector --verbose --country France --country Germany --country Greece --latest 50 --download-timeout 10 --age 72 --protocol https --protocol http --sort rate --save /etc/pacman.d/mirrorlist; cat /etc/pacman.d/mirrorlist'
alias hdmic='pactl set-source-port alsa_input.pci-0000_00_1f.3.analog-stereo analog-input-headset-mic'
alias inmic='pactl set-source-port alsa_input.pci-0000_00_1f.3.analog-stereo analog-input-internal-mic'
alias citra='flatpak run org.citra_emu.citra'
export EDITOR=/usr/bin/nvim
export BROWSER=/usr/bin/firefox
export TERM=xterm-256color
export MANPAGER="/bin/sh -c \"col -b | vim --not-a-term -c 'set ft=man ts=8 nomod nolist noma' -\""
export TERMINAL=alacritty
pfetch
