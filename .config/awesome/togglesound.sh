#Toggles between local speakers and hdmi output
#   ____ _  __
#  / ___| |/ /
# | |   | ' /
# | |___| . \
#  \____|_|\_\

#!/usr/bin/env bash

TOGGLE=$HOME/.config/awesome/.toggle
MON=$(hwinfo | grep AOC)

if [ ! -e $TOGGLE ] && [ "$MON" != "" ]; then
    touch $TOGGLE
    pactl set-card-profile 0 output:hdmi-stereo-extra1+input:analog-stereo
elif [ -e $TOGGLE ]; then
    rm $TOGGLE
    pactl set-card-profile 0 output:analog-stereo+input:analog-stereo
fi
