#!/usr/bin/env bash

function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

run /usr/bin/lxpolkit &
run picom &
systemctl --user import-environment
xrandr --output eDP-1-1 --mode 1920x1080 --pos 1920x0 --rotate normal --output DP-1-1 --off --output HDMI-1-1 --off --output HDMI-1-2 --primary --mode 1920x1080 --pos 0x0 --rotate normal --dpi 120
xrandr --dpi 120
xrdb ~/.Xresources
run xfce4-power-manager
run dunst & 
run nm-applet &
run pasystray --notify=systray_action &
run udiskie &
run xsetroot -cursor_name bibata_classic
run flameshot &
run /usr/lib/kdeconnectd
run kdeconnect-indicator &
numlockx on
TOGGLE=$HOME/.config/awesome/.toggle
MON=$(hwinfo | grep AOC)

if [ "$MON" == "" ] && [ -e "$TOGGLE" ]; then
    rm $TOGGLE
fi

if [ ! -e $TOGGLE ] && [ "$MON" != "" ]; then
    touch $TOGGLE
    pactl set-card-profile 0 output:hdmi-stereo-extra1+input:analog-stereo
fi
