#Toggles caps-lock indicator 
#   ____ _  __
#  / ___| |/ /
# | |   | ' /
# | |___| . \
#  \____|_|\_\

#!/bin/bash

CAPS_STATE=$(xset q)
PATTERN='Caps Lock:\s+(on|off)'

if [[ "$CAPS_STATE" =~ $PATTERN ]]; then
  # Match
  echo ${BASH_REMATCH[1]}
  exit 0
else
  # Nothing...
  exit 1
fi
