#!/bin/bash

get_active_monitors()
{
    xrandr | awk '/ connected/ && /[[:digit:]]x[[:digit:]].*+/{print $1}'
}

if [ $(get_active_monitors | wc -l) -eq 2 ] 
then
	flameshot screen -c -p ~/Pictures/screenshots
else
	flameshot full -c -p ~/Pictures/screenshots
fi
