--Just checks for .toggle, used by theme.lua to determine speaker state
--  ____ _  __
-- / ___| |/ /
--| |   | ' /
--| |___| . \
-- \____|_|\_\
function file_exists(name)
   local f=io.open(name,"r")
   if f~=nil then io.close(f) return true else return false end
end

a = file_exists("/home/christos/.config/awesome/.toggle")
print(a)
