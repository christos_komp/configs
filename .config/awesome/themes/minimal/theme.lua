-- My minimal theme
-- For low idle CPU usage
--
--
--   ____ _  __
--  / ___| |/ /
-- | |   | ' /
-- | |___| . \
--  \____|_|\_\


local gears = require("gears")
local lain  = require("lain")
local awful = require("awful")
local wibox = require("wibox")
local dpi   = require("beautiful.xresources").apply_dpi
local logout = require("awesome-wm-widgets.logout-widget.logout")
local awesomebuttons = require("awesome-buttons.awesome-buttons")
local os = os
local common = require("awful.widget.common")
local my_table = awful.util.table or gears.table -- 4.{0,1} compatibility
local theme                                     = {}
theme.confdir                                   = os.getenv("HOME") .. "/.config/awesome/themes/minimal"
theme.wallpaper                                 = theme.confdir .. "/wall.jpg"

theme.font                                      = "Sen Regular 10"
theme.icon_theme				= "Papirus"
theme.menu_bg_normal                            = "#1e1f29"
theme.menu_bg_focus                             = "#1e1f29"
theme.bg_normal                                 = "#1e1f29"
theme.bg_focus                                  = "#1e1f29"
theme.bg_urgent                                 = "#1e1f29"
theme.fg_normal                                 = "#ffffff"
theme.fg_focus                                  = "#6d5890"
theme.fg_urgent                                 = "#ff0000"
theme.fg_minimize                               = "#FFFFFF"
theme.border_width                              = dpi(1)
theme.border_normal                             = "#1e1f29"
theme.border_focus                              = "#6d5890"
theme.border_marked                             = "#3ca4d8"
theme.menu_border_width                         = 0
theme.menu_width                                = dpi(130)
theme.menu_submenu_icon                         = theme.confdir .. "/icons/submenu.png"
theme.menu_fg_normal                            = "#ffffff"
theme.menu_fg_focus                             = "#6d5890"
theme.menu_bg_normal                            = "#1e1f29"
theme.menu_bg_focus                             = "#1e1f29"
theme.widget_temp                               = theme.confdir .. "/icons/temp.png"
theme.widget_uptime                             = theme.confdir .. "/icons/ac.png"
theme.widget_cpu                                = theme.confdir .. "/icons/cpu.png"
theme.widget_weather                            = theme.confdir .. "/icons/dish.png"
theme.widget_fs                                 = theme.confdir .. "/icons/fs.png"
theme.widget_mem                                = theme.confdir .. "/icons/mem.png"
theme.widget_note                               = theme.confdir .. "/icons/note.png"
theme.widget_note_on                            = theme.confdir .. "/icons/note_on.png"
theme.widget_netdown                            = theme.confdir .. "/icons/net_down.png"
theme.widget_netup                              = theme.confdir .. "/icons/net_up.png"
theme.widget_mail                               = theme.confdir .. "/icons/mail.png"
theme.widget_batt                               = theme.confdir .. "/icons/bat.png"
theme.widget_clock                              = theme.confdir .. "/icons/clock.png"
theme.widget_play                               = theme.confdir .. "/icons/media-playback-start.png"
theme.widget_pause                              = theme.confdir .. "/icons/media-playback-pause.png"
theme.widget_vol                                = theme.confdir .. "/icons/spkr.png"
theme.taglist_squares_sel                       = theme.confdir .. "/icons/square_a.png"
theme.taglist_squares_unsel                     = theme.confdir .. "/icons/square_b.png"
theme.tasklist_plain_task_name                  = false
theme.tasklist_disable_icon                     = false
theme.tasklist_disable_task_name                = false
theme.tasklist_bg_normal                        = "#6d5890"
theme.tasklist_fg_normal                        = "#ff79c6"
theme.tasklist_bg_focus                         = "#6d5890"
theme.tasklist_fg_focus                         = "#ffffff"
theme.tasklist_bg_minimize                      = "#1e1f29"
theme.tasklist_spacing				= 4
theme.tasklist_shape_minimized			= gears.shape.rounded_rect
theme.taglist_spacing                           = 0
theme.useless_gap                               = 0
theme.layout_tile                               = theme.confdir .. "/icons/tile.png"
theme.layout_tilegaps                           = theme.confdir .. "/icons/tilegaps.png"
theme.layout_tileleft                           = theme.confdir .. "/icons/tileleft.png"
theme.layout_tilebottom                         = theme.confdir .. "/icons/tilebottom.png"
theme.layout_tiletop                            = theme.confdir .. "/icons/tiletop.png"
theme.layout_fairv                              = theme.confdir .. "/icons/fairv.png"
theme.layout_fairh                              = theme.confdir .. "/icons/fairh.png"
theme.layout_spiral                             = theme.confdir .. "/icons/spiral.png"
theme.layout_dwindle                            = theme.confdir .. "/icons/dwindle.png"
theme.layout_max                                = theme.confdir .. "/icons/max.png"
theme.layout_fullscreen                         = theme.confdir .. "/icons/fullscreen.png"
theme.layout_magnifier                          = theme.confdir .. "/icons/magnifier.png"
theme.layout_floating                           = theme.confdir .. "/icons/floating.png"
theme.titlebar_close_button_normal              = theme.confdir .. "/icons/titlebar/close_normal.png"
theme.titlebar_close_button_focus               = theme.confdir .. "/icons/titlebar/close_focus.png"
theme.titlebar_minimize_button_normal           = theme.confdir .. "/icons/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus            = theme.confdir .. "/icons/titlebar/minimize_focus.png"
theme.titlebar_ontop_button_normal_inactive     = theme.confdir .. "/icons/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive      = theme.confdir .. "/icons/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active       = theme.confdir .. "/icons/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active        = theme.confdir .. "/icons/titlebar/ontop_focus_active.png"
theme.titlebar_sticky_button_normal_inactive    = theme.confdir .. "/icons/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive     = theme.confdir .. "/icons/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active      = theme.confdir .. "/icons/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active       = theme.confdir .. "/icons/titlebar/sticky_focus_active.png"
theme.titlebar_floating_button_normal_inactive  = theme.confdir .. "/icons/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive   = theme.confdir .. "/icons/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active    = theme.confdir .. "/icons/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active     = theme.confdir .. "/icons/titlebar/floating_focus_active.png"
theme.titlebar_maximized_button_normal_inactive = theme.confdir .. "/icons/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = theme.confdir .. "/icons/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active   = theme.confdir .. "/icons/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active    = theme.confdir .. "/icons/titlebar/maximized_focus_active.png"

local markup = lain.util.markup

-- Textclock
os.setlocale(os.getenv("LANG")) -- to localize the clock
local clockicon = wibox.widget.imagebox(theme.widget_clock)
local mytextclock = wibox.widget.textclock( markup.fontfg( theme.font, theme.fg_focus,"%a %d %b" .. " %H:%M"))
mytextclock.refresh = 5

function list_update(w, buttons, label, data, objects)
    -- call default widget drawing function
    common.list_update(w, buttons, label, data, objects)
    -- set widget size
    w:set_max_widget_size(20)
end

theme.cal = lain.widget.cal({
    attach_to = { mytextclock },
    followtag = true,
    notification_preset = {
        font = "Roboto Mono 10",
        fg   = theme.fg_normal,
        bg   = theme.bg_normal
    }
})

--kernel version
local kernel = awful.widget.watch('uname -r', 10000, function(widget, stdout)
    for line in stdout:gmatch("[^\r\n]+") do
        if line:match("-") then
            widget:set_markup(markup.fontfg(theme.font, theme.fg_normal, line .. " " ))
            return
        end
    end
end)

--package updates
local pacupdates = awful.widget.watch('bash -c "checkupdates | wc -l"', 30, function(widget, stdout)
    widget:set_markup(markup.fontfg(theme.font, theme.fg_focus, stdout .. " "))
end)

-- caps lock indicator
local caps = awful.widget.watch('bash /home/christos/.config/awesome/togglecaps.sh', 0.8, function(widget, stdout)
    for line in stdout:gmatch("[^\r\n]+") do
        if line:match("on") then
            widget:set_markup(markup.font(theme.font, "⬆"))
            return
        else
            widget:set_markup(markup.font(theme.font, ""))
            return
        end
    end
end)

-- CPU
local cpuicon = wibox.widget.imagebox(theme.widget_cpu)
local cpu = lain.widget.cpu({
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, theme.fg_normal, cpu_now.usage .. "%  "))
    end
})


-- MEM
local memicon = wibox.widget.imagebox(theme.widget_mem)
local memory = lain.widget.mem({
    settings = function( )
        widget:set_markup(markup.fontfg(theme.font, theme.fg_focus, mem_now.used .. " MB  "))
    end
})

-- Net
local netdownicon = wibox.widget.imagebox(theme.widget_netdown)
local netdowninfo = wibox.widget.textbox()
local netupicon = wibox.widget.imagebox(theme.widget_netup)
local netupinfo = lain.widget.net({
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, theme.fg_normal, net_now.sent .. " "))
        netdowninfo:set_markup(markup.fontfg(theme.font, theme.fg_focus, net_now.received .. " "))
    end
})


local spr = wibox.widget.textbox(' ')
local pacicon = wibox.widget.textbox(' ')
pacicon:set_markup(markup.fontfg("Sen Regular 9", theme.fg_focus,"  "))
local kericon = wibox.widget.textbox(' ')
kericon:set_markup(markup.fontfg("Sen Regular 9", theme.fg_normal, "   "))
function theme.at_screen_connect(s)
    -- Quake application
    s.quake = lain.util.quake({ app = awful.util.terminal })

    -- If wallpaper is a function, call it with the screen
    local wallpaper = theme.wallpaper
    if type(wallpaper) == "function" then
        wallpaper = wallpaper(s)
    end
    gears.wallpaper.maximized(wallpaper, s, false)

    --awful.spawn.with_shell('setrandom /usr/share/background/archselect/')
    -- Tags
    awful.tag({ "I", "II", "III", "IV"}, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(my_table.join(
                           awful.button({}, 1, function () awful.layout.inc( 1) end),
                           awful.button({}, 2, function () awful.layout.set( awful.layout.layouts[1] ) end),
                           awful.button({}, 3, function () awful.layout.inc(-1) end),
                           awful.button({}, 4, function () awful.layout.inc( 1) end),
                           awful.button({}, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, awful.util.taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = wibox.container.constraint(awful.widget.tasklist{
     screen   = s,
     filter   = awful.widget.tasklist.filter.currenttags,
     buttons  = awful.util.tasklist_buttons,
     style    = {
         shape_border_width = 1,
         shape_border_color = '#777777',
         shape  = gears.shape.rounded_rect,
     },
     layout   = {
         spacing = 10,
         spacing_widget = {
             {
                 forced_width = 5,
                 shape        = gears.shape.circle,
                 widget       = wibox.widget.separator
             },
             valign = 'center',
             halign = 'center',
             widget = wibox.container.place,
         },
         layout  = wibox.layout.flex.horizontal
     },
     -- Notice that there is *NO* wibox.wibox prefix, it is a template,
     -- not a widget instance.
     widget_template = {
         {
             {
                 {
                     {
                         id     = 'icon_role',
                         widget = wibox.widget.imagebox,
                     },
                     margins = 2,
                     widget  = wibox.container.margin,
                 },
                 {
                     id     = 'text_role',
                     widget = wibox.widget.textbox,
                 },
                 layout = wibox.layout.fixed.horizontal,
             },
             left  = 10,
             right = 10,
             widget = wibox.container.margin
         },
         id     = 'background_role',
         widget = wibox.container.background,
      },
    }, "max", 900, nil )

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s, height = dpi(18), bg = theme.bg_normal, fg = theme.fg_normal })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            expand = "none",
            layout = wibox.layout.fixed.horizontal,
            --s.mylayoutbox,
            awesomebuttons.with_icon_and_text{ icon = 'grid', color = '#fff', text = 'Run',  onclick = function() awful.util.spawn('/home/christos/.config/rofi/launchers/colorful/launcher.sh') end, shape = 'rounded_rect'},
	    s.mytaglist,
            s.mypromptbox,
            spr,
            s.mytasklist, -- Middle widget
	    nil,
        },

           -- layout = wibox.layout.fixed.horizontal,
           nil,

        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            s.mylayoutbox,
            caps,
            mykeyboardlayout,
            wibox.widget.systray(),
            netdownicon,
            netdowninfo,
            netupicon,
            netupinfo.widget,
            pacicon,
	    pacupdates,
	    kericon,
            kernel,
            memicon,
            memory.widget,
            cpuicon,
            cpu.widget,
            mytextclock,
            spr,
            logout.widget{accent_color = "#ff79c6", phrases = {"Goodbye!"},},
        },
    }
end
return theme
