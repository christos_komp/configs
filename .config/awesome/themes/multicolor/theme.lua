-- My Multicolor version
--   ____ _  __
--  / ___| |/ /
-- | |   | ' /
-- | |___| . \
--  \____|_|\_\

--local spotify_widget = require("../../awesome-wm-widgets.spotify-widget.spotify")
--local brightness_widget = require("../../awesome-wm-widgets.brightness-widget.brightness")

local gears = require("gears")
local lain  = require("lain")
local awful = require("awful")
local wibox = require("wibox")
local dpi   = require("beautiful.xresources").apply_dpi

local os = os
local my_table = awful.util.table or gears.table -- 4.{0,1} compatibility

local theme                                     = {}
theme.confdir                                   = os.getenv("HOME") .. "/.config/awesome/themes/multicolor"
theme.wallpaper                                 = theme.confdir .. "/wall.png"
theme.font                                      = "Roboto Regular 10"
theme.menu_bg_normal                            = "#2f343f"
theme.menu_bg_focus                             = "#2f343f"
theme.bg_normal                                 = "#2f343f"
theme.bg_focus                                  = "#2f343f"
theme.bg_urgent                                 = "#2f343f"
theme.fg_normal                                 = "#bebebe"
theme.fg_focus                                  = "#5294e2"
theme.fg_urgent                                 = "#ff0000"
theme.fg_minimize                               = "#FFFFFF"
theme.border_width                              = dpi(1)
theme.border_normal                             = "#2f343f"
theme.border_focus                              = "#4084d6"
theme.border_marked                             = "#3ca4d8"
theme.menu_border_width                         = 0
theme.menu_width                                = dpi(130)
theme.menu_submenu_icon                         = theme.confdir .. "/icons/submenu.png"
theme.menu_fg_normal                            = "#bebebe"
theme.menu_fg_focus                             = "#4084d6"
theme.menu_bg_normal                            = "#2f343f"
theme.menu_bg_focus                             = "#2f343f"
theme.widget_temp                               = theme.confdir .. "/icons/temp.png"
theme.widget_uptime                             = theme.confdir .. "/icons/ac.png"
theme.widget_cpu                                = theme.confdir .. "/icons/cpu.png"
theme.widget_weather                            = theme.confdir .. "/icons/dish.png"
theme.widget_fs                                 = theme.confdir .. "/icons/fs.png"
theme.widget_mem                                = theme.confdir .. "/icons/mem.png"
theme.widget_note                               = theme.confdir .. "/icons/note.png"
theme.widget_note_on                            = theme.confdir .. "/icons/note_on.png"
theme.widget_netdown                            = theme.confdir .. "/icons/net_down.png"
theme.widget_netup                              = theme.confdir .. "/icons/net_up.png"
theme.widget_mail                               = theme.confdir .. "/icons/mail.png"
theme.widget_batt                               = theme.confdir .. "/icons/bat.png"
theme.widget_clock                              = theme.confdir .. "/icons/clock.png"
theme.widget_play                               = theme.confdir .. "/icons/media-playback-start.png"
theme.widget_pause                              = theme.confdir .. "/icons/media-playback-pause.png"
theme.widget_vol                                = theme.confdir .. "/icons/spkr.png"
theme.taglist_squares_sel                       = theme.confdir .. "/icons/square_a.png"
theme.taglist_squares_unsel                     = theme.confdir .. "/icons/square_b.png"
theme.tasklist_plain_task_name                  = true
theme.tasklist_disable_icon                     = true
theme.useless_gap                               = 0
theme.layout_tile                               = theme.confdir .. "/icons/tile.png"
theme.layout_tilegaps                           = theme.confdir .. "/icons/tilegaps.png"
theme.layout_tileleft                           = theme.confdir .. "/icons/tileleft.png"
theme.layout_tilebottom                         = theme.confdir .. "/icons/tilebottom.png"
theme.layout_tiletop                            = theme.confdir .. "/icons/tiletop.png"
theme.layout_fairv                              = theme.confdir .. "/icons/fairv.png"
theme.layout_fairh                              = theme.confdir .. "/icons/fairh.png"
theme.layout_spiral                             = theme.confdir .. "/icons/spiral.png"
theme.layout_dwindle                            = theme.confdir .. "/icons/dwindle.png"
theme.layout_max                                = theme.confdir .. "/icons/max.png"
theme.layout_fullscreen                         = theme.confdir .. "/icons/fullscreen.png"
theme.layout_magnifier                          = theme.confdir .. "/icons/magnifier.png"
theme.layout_floating                           = theme.confdir .. "/icons/floating.png"
theme.titlebar_close_button_normal              = theme.confdir .. "/icons/titlebar/close_normal.png"
theme.titlebar_close_button_focus               = theme.confdir .. "/icons/titlebar/close_focus.png"
theme.titlebar_minimize_button_normal           = theme.confdir .. "/icons/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus            = theme.confdir .. "/icons/titlebar/minimize_focus.png"
theme.titlebar_ontop_button_normal_inactive     = theme.confdir .. "/icons/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive      = theme.confdir .. "/icons/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active       = theme.confdir .. "/icons/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active        = theme.confdir .. "/icons/titlebar/ontop_focus_active.png"
theme.titlebar_sticky_button_normal_inactive    = theme.confdir .. "/icons/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive     = theme.confdir .. "/icons/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active      = theme.confdir .. "/icons/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active       = theme.confdir .. "/icons/titlebar/sticky_focus_active.png"
theme.titlebar_floating_button_normal_inactive  = theme.confdir .. "/icons/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive   = theme.confdir .. "/icons/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active    = theme.confdir .. "/icons/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active     = theme.confdir .. "/icons/titlebar/floating_focus_active.png"
theme.titlebar_maximized_button_normal_inactive = theme.confdir .. "/icons/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = theme.confdir .. "/icons/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active   = theme.confdir .. "/icons/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active    = theme.confdir .. "/icons/titlebar/maximized_focus_active.png"

local markup = lain.util.markup

-- Textclock
os.setlocale(os.getenv("LANG")) -- to localize the clock
local clockicon = wibox.widget.imagebox(theme.widget_clock)
local mytextclock = wibox.widget.textclock(markup("#7788ff", "%a %d %b ") .. markup("#ff5e1e", "%H:%M"))
mytextclock.font = theme.font
mytextclock.refresh = 5


theme.cal = lain.widget.cal({
    attach_to = { mytextclock },
    followtag = true,
    notification_preset = {
        font = "Roboto Mono Regular 10",
        fg   = theme.fg_normal,
        bg   = theme.bg_normal
    }
})

--kernel version
local kernel = awful.widget.watch('uname -r', 10000, function(widget, stdout)
    for line in stdout:gmatch("[^\r\n]+") do
        if line:match("-") then
            widget:set_markup(markup.fontfg(theme.font, "#ffa4c4", "  " .. line .. " " ))
            return
        end
    end
end)

--package number
local pacnumicon = wibox.widget.textbox("")
pacnumicon:set_markup(markup.fontfg('Roboto Regular 8', "#ffaf5f", "  "))
local pacnum = awful.widget.watch('bash -c "yay -Q | wc -l"', 30, function(widget, stdout)
    widget:set_markup(markup.fontfg(theme.font, "#ffaf5f",  stdout .. " "))
end)

--package updates
local pacupicon = wibox.widget.textbox("")
pacupicon:set_markup(markup.fontfg('Roboto Regular 8', "#ffaf5f", "   "))
local pacupdates = awful.widget.watch('bash -c "checkupdates | wc -l"', 20, function(widget, stdout)
    widget:set_markup(markup.fontfg(theme.font, "#ffaf5f", stdout .. " "))
end)

--get sound output device
local sound = awful.widget.watch('lua /home/christos/.config/awesome/spkrstate.lua', 1, function(widget, stdout)
    for line in stdout:gmatch("[^\r\n]+") do
        if line:match("true") then
            widget:set_markup(markup.fontfg(theme.font, "#7493ff", "HDMI @ "))
            return
        else
            widget:set_markup(markup.fontfg(theme.font, "#7493ff", "Spkrs @ "))
            return
        end
    end
end)

-- caps lock indicator
local caps = awful.widget.watch('bash /home/christos/.config/awesome/togglecaps.sh', 0.8, function(widget, stdout)
    for line in stdout:gmatch("[^\r\n]+") do
        if line:match("on") then
            widget:set_markup(markup.font(theme.font, "⬆"))
            return
        else
            widget:set_markup(markup.font(theme.font, ""))
            return
        end
    end
end)

-- / fs
local fsicon = wibox.widget.imagebox(theme.widget_fs)
theme.fs = lain.widget.fs({
    notification_preset = { font = "Roboto Mono Regular 10", fg = theme.fg_normal,
},
    timeout = 15,
    followtag = true,
    settings  = function()
        widget:set_markup(markup.fontfg(theme.font, "#80d9ff", string.format("%.1f", fs_now["/"].percentage) .. "% "))
    end
})


-- CPU
local cpuicon = wibox.widget.imagebox(theme.widget_cpu)
local cpu = lain.widget.cpu({
    timeout = 3,
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, "#ff3a6e", cpu_now.usage .. "% "))
    end
})

-- Coretemp
local tempicon = wibox.widget.imagebox(theme.widget_temp)
local temp = lain.widget.temp({
    timeout = 3,
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, "#ffaf5f", coretemp_now .. "°C "))
    end
})
-- Battery
local baticon = wibox.widget.imagebox(theme.widget_batt)
local bat = lain.widget.bat({
    timeout = 25,
    settings = function()
        local perc = bat_now.perc ~= "N/A" and bat_now.perc .. "%" or bat_now.perc

        if bat_now.ac_status == 1 then
            perc = perc .. " plug"
        end

        widget:set_markup(markup.fontfg(theme.font, theme.fg_normal, perc .. " "))
    end
})

-- Weather
local weathericon = wibox.widget.imagebox(theme.widget_weather)
theme.weather = lain.widget.weather({
    followtag = true,
    timeout = 900,
    city_id = 258620, -- Lamia
    notification_preset = { font = "Roboto Mono Regular 10", fg = theme.fg_normal },
    weather_na_markup = markup.fontfg(theme.font, "#ffa4c4", "N/A "),
    settings = function()
        descr = weather_now["weather"][1]["description"]:lower()
        units = math.floor(weather_now["main"]["temp"])
        widget:set_markup(markup.fontfg(theme.font, "#ffa4c4", descr .. " @ " .. units .. "°C"))
    end
})

-- ALSA volume
local volicon = wibox.widget.imagebox(theme.widget_vol)
theme.volume = lain.widget.alsa({
    timeout = 0.8,
    settings = function()
        if volume_now.status == "off" then
            widget:set_markup(markup.fontfg(theme.font, "#7493ff", volume_now.level .. "X "))
        else
            widget:set_markup(markup.fontfg(theme.font, "#7493ff", volume_now.level .. "% "))
        end

    end
})

theme.volume.widget:buttons(awful.util.table.join(
                               awful.button({}, 4, function ()
                                     awful.util.spawn("amixer set Master 5%+")
                                     theme.volume.update()
                               end),
                               awful.button({}, 5, function ()
                                     awful.util.spawn("amixer set Master 5%-")
                                     theme.volume.update()
                               end),
                               awful.button({}, 1, function ()
                                     awful.util.spawn("amixer set Master toggle")
                                     theme.volume.update()
                               end),
                               awful.button({}, 3, function ()
                                     awful.util.spawn("amixer set Master toggle")
                                     theme.volume.update()
                               end)
))


-- Net
local netdownicon = wibox.widget.imagebox(theme.widget_netdown)
local netdowninfo = wibox.widget.textbox()
local netupicon = wibox.widget.imagebox(theme.widget_netup)
local netupinfo = lain.widget.net({
    timeout = 2,
    settings = function()

        widget:set_markup(markup.fontfg(theme.font, "#ff4c62", net_now.sent .. " "))
        netdowninfo:set_markup(markup.fontfg(theme.font, "#87ff5f", net_now.received .. " "))
    end
})

-- MEM
local memicon = wibox.widget.imagebox(theme.widget_mem)
local memory = lain.widget.mem({
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, "#ffff37", mem_now.used .. "M "))
    end
})


local spr = wibox.widget.textbox(' ')

function theme.at_screen_connect(s)
    -- Quake application
    s.quake = lain.util.quake({ app = awful.util.terminal })

    -- If wallpaper is a function, call it with the screen
    local wallpaper = theme.wallpaper
    if type(wallpaper) == "function" then
        wallpaper = wallpaper(s)
    end
    gears.wallpaper.maximized(wallpaper, s, false)

    -- Tags
   -- awful.tag(awful.util.tagnames, s, awful.layout.layouts[1])
    --awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9"}, s, awful.layout.layouts[1])
    awful.tag({ "", "", "", "", "", "", "", "", ""}, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(my_table.join(
                           awful.button({}, 1, function () awful.layout.inc( 1) end),
                           awful.button({}, 2, function () awful.layout.set( awful.layout.layouts[1] ) end),
                           awful.button({}, 3, function () awful.layout.inc(-1) end),
                           awful.button({}, 4, function () awful.layout.inc( 1) end),
                           awful.button({}, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, awful.util.taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, awful.util.tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s, height = dpi(18), bg = theme.bg_normal, fg = theme.fg_normal })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            --s.mylayoutbox,
            s.mytaglist,
            s.mypromptbox,
        },
        --s.mytasklist, -- Middle widget
        nil,
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            caps,
            mykeyboardlayout,
            --brightness_widget(),
            --spr,
            wibox.widget.systray(),
            netdownicon,
            netdowninfo,
            netupicon,
            netupinfo.widget,
            volicon,
            sound,
            theme.volume.widget,
            --spotify_widget(),
            kernel,
            pacnumicon,
            pacnum,
            pacupicon,
            pacupdates,
            memicon,
            memory.widget,
            cpuicon,
            cpu.widget,
            tempicon,
            temp,
            baticon,
            bat.widget,
            fsicon,
            theme.fs.widget,
            weathericon,
            theme.weather.widget,
            clockicon,
            mytextclock,
            spr,
            s.mylayoutbox,
        },
    }
end
return theme
