#!/usr/bin/env bash

declare options=("
arandr
etcher
baobab
bashtop
font-manager
gparted
htop
kvantum
lightdm-greeter
lxappearance
matlab
pavucontrol
qalculate
qt5ct
simple-scan
xev
")

choice=$(echo -e "${options[@]}" | dmenu -i -p 'Utilities: ')


case $choice in
	quit)
		echo "Program terminated." && exit 1
	;;
    bashtop| \
	htop)
        exec termite -e "$choice"
	;;
	arandr)
        exec arandr
	;;
    etcher)
        exec etcher
	;;

	font-manager)
        exec font-manager
	;;
	gparted)
        exec gparted
	;;
	kvantum)
        exec kvantummanager
	;;
	matlab)
	    exec matlab
    	;;
	pavucontrol)
        exec pavucontrol
	;;
	baobab)
        exec baobab
	;;
	simple-scan)
        exec simple-scan
	;;
    	qalculate)
        exec qalculate-gtk
	;;
    	qt5ct)
        exec qt5ct
	;;
	lxappearance)
        exec lxappearance
	;;
	lightdm-greeter)
        exec lightdm-gtk-greeter-settings-pkexec
	;;
	xev)
        termite -e "xev"
	;;
	*)
		exit 1
	;;
esac
