#!/usr/bin/env bash

declare options=("
termite
bashrc
vimrc
rc.lua
autostart
dmenu/configs
dmenu/utilities
minimal/theme.lua
multicolor/theme.lua
powerarrow/theme.lua
xresources
picom.conf
")

choice=$(echo -e "${options[@]}" | dmenu -i -p 'Edit config file: ')

case "$choice" in
    termite)
		choice="$HOME/.config/termite/config"
	;;
    bashrc)
		choice="$HOME/.bashrc"
	;;
    vimrc)
		choice="$HOME/.vimrc"
	;;
    rc.lua)
		choice="$HOME/.config/awesome/rc.lua"
	;;
    autostart)
		choice="$HOME/.config/awesome/autorun.sh"
	;;
    dmenu/configs)
		choice="$HOME/.dmenu/dmenu-configs.sh"
	;;
    dmenu/utilities)
		choice="$HOME/.dmenu/dmenu-utilities.sh"
	;;
    multicolor/theme.lua)
		choice="$HOME/.config/awesome/themes/multicolor/theme.lua"
	;;
    minimal/theme.lua)
		choice="$HOME/.config/awesome/themes/minimal/theme.lua"
	;;
    powerarrow/theme.lua)
		choice="$HOME/.config/awesome/themes/powerarrow-dark/theme.lua"
	;;
    xresources)
		choice="$HOME/.Xresources"
	;;
    picom.conf)
		choice="$HOME/.config/picom.conf"
	;;
    *)
		exit 1
	;;
esac
  termite -e "vimdel vim $choice"
